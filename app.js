"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    Node server
*/

const CONFIG = require("./Config/config");
const http = require('http');
//var https = require('https');
const fs = require('fs');

const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();
const compression = require('compression');
const cors = require('cors')

const RedisServer = require('redis-server');

const indexRouter = require('./Routes/index');
const validation = require('./Middleware/validation');
const cluster = require('./cluster');
const app = express();

const corsOptions = {
  origin: '*',
  methods: 'GET, PUT, POST, DELTET'
}
app.use(cors(corsOptions));
app.use('/static', express.static('public'));
app.use(compression())
// for parsing application/json
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true })); 

//Middleware addition
app.use(validation.authenticate);


app.use('/', indexRouter);
app.use('/api/v1/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).send('Not found');
});

 app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Internal server error');
 });

 const checkCredsForDB = () => {
  if (
      CONFIG.DB_USER_NAME == "{DB_UserName}" ||
      CONFIG.DB_USER_NAME == undefined ||
      CONFIG.DB_PASSWORD == undefined ||
      CONFIG.DB_PASSWORD == "{DB_Password}"
  ) {
      console.log("Please set DB_USER_NAME and DB_PASSWORD in config/config.js file");
      process.exit(1);
  }
};

 
//Setting up server
//app.set('port', CONFIG.SERVER_PORT);
let cert = "", key = "";
 if(CONFIG.KEY_FILE_PATH && CONFIG.CERT_FILE_PATH) {
    key = fs.readFileSync(CONFIG.KEY_FILE_PATH);
   cert = fs.readFileSync(CONFIG.CERT_FILE_PATH);
}
const option = {cert, key, ca: null}

checkCredsForDB();

cluster.createServer(app, option);

startRedisServer();
function startRedisServer() {
  const server = new RedisServer(6379);
  server.open((err) => {
    if (err === null) {
          console.log("redis server is running")
    }
  });
}
  