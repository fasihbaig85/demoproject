"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    Common utilities 
*/

const STATUS_CODES = require("./ENUMS");

module.exports = {
    sendResponse: function(res = null, statusCode = 0, message = "", data = null) {
        let resBody = { statusCode: 0, message: ""}
        if(!res && typeof res != "object") {
            console.error("Invalid res object");
            return;
        }

        if(statusCode) {
            resBody.statusCode = statusCode;
        }

        if(statusCode && !isNaN(statusCode)) {
            resBody.statusCode = Number(statusCode);
        }

        if(message && typeof message == "string") {
            resBody.message = message;
        }

        resBody.data = data || ""
        res.send(resBody);
    },

    validateEmail: function(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
}