"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    ENUMS
*/

var enums = {};

enums.responseCode = {
    OK: 0,
    INTERNAL_SERVER_ERROR: 500,
}

module.exports = enums