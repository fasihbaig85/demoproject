"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    Middleware token validation
*/

const jwt = require("jsonwebtoken");
const CONFIG = require("../Config/config");
const redisClient = require("../Database/redisClient");

/**
 * validating token in authenticate
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function authenticate(req, res, next) {
    if(req.path.indexOf("/signin") > 0 || req.path.indexOf("/signup") > 0) {
        next();
        return;
    }

    let token = req.headers.token;
    if(!token) {
        res.status(403).send("Unauthorized");
        return;
    }

    try {
        jwt.verify(token, CONFIG.JWT_SECRET_KEY, function(err, decoded) {
            if(err || !decoded || !decoded.id || !decoded.username) {
                res.status(403).send("Unauthorized"); 
                return;
            }
            req.user = decoded
            redisClient.getClient().get('token_' + token, (err, reply) => {
                if(err) {
                    res.statis(403).send("Unauthorized"); 
                    return;
                }

                if(!reply) {
                    res.send({
                        status: 3000,
                        message: "token expired"
                    }); 
                    return;
                }

                if(reply != req.user.id) {
                    res.status(403).send("Unauthorized");
                    return;
                }
                next();
            })
        });
    } catch(err) {
        res.status(403).send("Unauthorized");
    }
}

module.exports = {authenticate};