var mysql = require('mysql');
var CONFIG  = require("../Config/config");
var pool  = mysql.createPool({
  connectionLimit : CONFIG.DB_POOL_SIZE,
  host            : CONFIG.DB_HOST,
  user            : CONFIG.DB_USER_NAME,
  password        : CONFIG.DB_PASSWORD,
  database        : CONFIG.DB_NAME,
  port            : CONFIG.DB_PORT,
  debug           : false,
  multipleStatements: true
});

const DB = {
    getConnection: function() {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if(err) {
            reject(err);
            return;
          }
          resolve(connection);
        })
      })
    }
}
module.exports = DB;