var redis = require('redis');

var redisClient = (function() {
    var client = null;
    var getClient = function(){
        if(client == null){
            client = redis.createClient();

            client.on('error', function(err){
                console.log("Error in creating Redis client.");
            });

            client.on('ready', function(){
                console.log("Success in creating Redis client.");
            });
        }
        return client;
    };

    return {
        getClient: getClient,
    }
})();

module.exports = redisClient;