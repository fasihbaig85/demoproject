"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    Artical Controller
*/

const Artical = require("../Models/Artical");
const md5 = require('md5');
const jwt = require("jsonwebtoken");
const CONFIG = require("../Config/config");
const redisClient = require("../Database/redisClient");
const multer = require("multer");
const path = require('path');
const fs = require('fs');
const ENUMS = require("../UTILITY/ENUMS");
const utility = require('../UTILITY/commonUtility');

const ArticalController = {
    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    getArtical: async function(req, res) {
        try {
            let id = req.query.id
            let articalObj = new Artical();
            let result = await articalObj.getArtical(id);
            if(result === -1) {
                utility.sendResponse(res, -4201, "Artical not fount");
            } else {
                utility.sendResponse(res, ENUMS.responseCode.OK,"", result); 
            } 
        } catch(err) {
            console.error(err);
            utility.sendResponse(res, -4005, "Unable to get articals");
        }
    },

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    addArtical: async function(req, res) {
        try {
            if (req.content) {
                utility.sendResponse(res, -4000, 'Invalid content');
                return;
            }
            
            if(!req.body.aurthorId || req.body.aurthorId <= 0) {
                utility.sendResponse(res, -4002, 'Invalid aurthor ID');
                return;
            }

            if(!req.body.articalTitle || req.body.articalTitle.length > 500) {
                utility.sendResponse(res, -4004, 'Invalid artical title characters should be 1 - 500');
                return;
            }

            if(req.user.id != req.body.aurthorId) {
                utility.sendResponse(res, -4004, 'Not authorized to post artical with different id');
                return;
            }

            let articalObj = new Artical();
            let result = await articalObj.addArtical(req.body.articalTitle, req.body.content, req.body.aurthorId, new Date().getTime());
            if(result === -2) {
                utility.sendResponse(res, -4005, "Unable to add artical");
            } else if(result === -1) {
                utility.sendResponse(res, ENUMS.responseCode.INTERNAL_SERVER_ERROR, "Internal server error");
            } else {
                utility.sendResponse(res, ENUMS.responseCode.OK,"", result); 
            } 
        } catch(err) {
            console.error(err);
            utility.sendResponse(res, -4005, "Unable to add artical");
        }
    },

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    updateArtical: async function(req, res) {
        try {
            const updateContent = !!req.body.content;
            const updateAurthor = !!req.body.aurthorId
            const updateTiltle = !!req.body.articalTitle
            const id  = req.body.id;

            if(!id) {
                res.send({
                    status: 4011,
                    message: 'Invalid artical Id'
                });
                return;
            }

            if(updateAurthor && (!req.body.aurthorId || req.body.aurthorId <= 0)) {
                res.send({
                    status: 4002,
                    message: 'Invalid aurthor'
                });
                return;
            }

            if(updateTiltle && (!req.body.articalTitle || req.body.articalTitle.length > 500)) {
                res.send({
                    status: 4004,
                    message: 'Invalid artical title characters  1 - 500'
                });
                return;
            }

            let articalObj = new Artical();
            let prev = await articalObj.getArtical(id);
            if(prev.length == 0) {
                res.send({
                    status: 4008,
                    message: 'Artical not found'
                });
                return;
            }
            const articalTitle = (updateTiltle)? req.body.articalTitle: prev[0].articalTitle;
            const aurthorId = (updateAurthor)? req.body.aurthorId: prev[0].aurthorId;
            const content = (updateContent)? req.body.content: prev[0].content;
            const modifiedTimestamp = new Date().getTime();
            let result = await articalObj.updateArtical(id, articalTitle, aurthorId, content, modifiedTimestamp);
            if(result === -1) {
                utility.sendResponse(res, 4011, "Artical not found");  
            } else {
                utility.sendResponse(res, ENUMS.responseCode.OK, "Updated successfully", result); 
            }
        } catch(err) {
            console.error(err);
            utility.sendResponse(res, 4012, "Unable to update artical");  
        }
    },

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    deleteArtical: async function(req, res) {
        try {
            if (!req.params.ids || req.params.ids.split(',').length == 0) {
                utility.sendResponse(res, 4007, 'Invalid Ids to delete');
                return;
            }
            let articalObj = new Artical();
            let result = await articalObj.deleteArtical(req.params.ids.split(','), new Date().getTime());
            if(result == -1) {
                utility.sendResponse(res, 4007, "Unable delete artical/s");
            } else {
                utility.sendResponse(res, ENUMS.responseCode.OK, "", result);
            }
        } catch(err) {
            console.error(err);
            utility.sendResponse(res, 4007, "Unable delete artical/s");
        }
    }
    /*
    addArtical: async function(req, res) {
        try {
            if (req.fileValidationError) {
                res.send({
                    status: 4000,
                    message: 'Invalid file type'
                });
                return;
            }

            if (!req.file) {
                res.send({
                    status: 4001,
                    message: 'Please select any document file to upload'
                });
                return;
            }
            
            if(!req.body.aurthorId || req.body.aurthorId <= 0) {
                res.send({
                    status: 4002,
                    message: 'Invalid aurthor ID'
                });
                return;
            }

            if(!req.body.articalTitle || req.body.articalTitle.length > 500) {
                res.send({
                    status: 4004,
                    message: 'Invalid artical title characters should be 1 - 500'
                });
                return;
            }

            let articalObj = new Artical();
            let result = await articalObj.addArtical(req.body.articalTitle, req.file.path, req.body.aurthorId, new Date().getTime());
            if(result > 0) {
                res.send({
                    status: 0,
                    message: "added successfully",
                    data: {
                        id: result,
                        link: '/static/uploads/' + req.file.filename,
                        aurthorId: req.body.aurthorId,
                        articalTitle: req.body.articalTitle 
                    } 
                }); 
            } else {
                res.send({
                    status: 4005,
                    message:"Unable to add artical"
                });
            }
        } catch(err) {
            console.log(err);
            res.send({
                status: 4005,
                message:"Unable to add artical"
            });
        }
    },

    updateArtical: async function(req, res) {
        try {
            const updateFilePath = !!req.file;
            const updateAurthor = !!req.body.aurthorId
            const updateTiltle = !!req.body.articalTitle
            const id  = req.body.id;

            if(!id) {
                res.send({
                    status: 4011,
                    message: 'Invalid artical Id'
                });
                return;
            }

            if (updateFilePath && req.fileValidationError) {
                res.send({
                    status: 4000,
                    message: 'Invalid file type'
                });
                return;
            }

            if(updateAurthor && (!req.body.aurthorId || req.body.aurthorId <= 0)) {
                res.send({
                    status: 4002,
                    message: 'Invalid aurthor'
                });
                return;
            }

            if(updateTiltle && (!req.body.articalTitle || req.body.articalTitle.length > 500)) {
                res.send({
                    status: 4004,
                    message: 'Invalid artical title characters  1 - 500'
                });
                return;
            }

            let articalObj = new Artical();
            let prev = await articalObj.getArticalsByIds(id);
            if(prev.length == 0) {
                res.send({
                    status: 4008,
                    message: 'Artical not found'
                });
                return;
            }
            const articalTitle = (updateTiltle)? req.body.articalTitle: prev[0].articalTitle;
            const aurthorId = (updateAurthor)? req.body.aurthorId: prev[0].aurthorId;
            const filePath = (updateFilePath)? req.file.path: prev[0].filePath;
            const filename = (updateFilePath)? req.file.filename: prev[0].filePath.split(',').pop();
            let result = await articalObj.updateArtical(id, articalTitle, aurthorId, filePath);
            if(result > 0) {
                res.send({
                    status: 0,
                    message: "updated successfully",
                    data: {
                        id,
                        link: '/static/uploads/' + filename,
                        aurthorId: aurthorId,
                        articalTitle: articalTitle 
                    } 
                }); 
                if(updateFilePath) {
                    if(fs.existsSync(prev[0].filePath)) {
                        fs.unlink(prev[0].filePath, (err)=>{});
                    }
                }
            } else {
                res.send({
                    status: 4012,
                    message:"Unable to update artical"
                });
            }
        } catch(err) {
            console.log(err);
            res.send({
                status: 4012,
                message:"Unable to update artical"
            });
        }
    },

    deleteArtical: async function(req, res) {
        try {
            if (!req.params.ids || req.params.ids.split(',').length == 0) {
                res.send({
                    status: 4007,
                    message: 'Invalid Ids to delete'
                });
                return;
            }
            let articalObj = new Artical();
            let selectedArticals = await articalObj.getArticalsByIds(req.params.ids);
            if(selectedArticals.length == 0) {
                res.send({
                    status: 4008,
                    message: "Articals not found"  
                });
                return;
            }
            let result = await articalObj.deleteArtical(req.params.ids, new Date().getTime());
            if(result > 0) {
                res.send({
                    status: 0,
                    message: "Successfully deleted" 
                });
                for(const item in selectedArticals) {
                    if(fs.existsSync(selectedArticals[item].filePath)) {
                        fs.unlink(selectedArticals[item].filePath, (err)=>{});
                    }
                }
            } else {
                res.send({
                    status: 4009,
                    message: "Unable to delete deleted" 
                });
            }
        } catch(err) {
            console.log(err);
            res.send({
                status: 4009,
                message:"Unable delete artical"
            });
        }
    },

    getMulterConfiguration() {
        const storage = multer.diskStorage({
            destination: function(req, file, cb) {
                cb(null, './public/uploads/');
            },
        
            filename: function(req, file, cb) {
                cb(null, file.fieldname + '_artical_' + Date.now() + path.extname(file.originalname));
            },
            limits:1024 * 10
          });
          
        return multer({ storage: storage, fileFilter: function(req, file, cb) {
                                    if (!file.originalname.match(/\.(pdf|doc|docx|txt)$/)) {
                                        req.fileValidationError = 'Only pdf/doc/docx/txt files are allowed!';
                                        return cb(new Error('Only pdf/doc/docx/txt files are allowed!'), false);
                                    }
                                    cb(null, true);
                                }
                            }).single('file');
    }
    */
}

module.exports = ArticalController;