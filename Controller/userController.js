"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    User Controller
*/

const User = require("../Models/User");
const md5 = require('md5');
const jwt = require("jsonwebtoken");
const CONFIG = require("../Config/config");
const redisClient = require("../Database/redisClient");
const ENUMS = require("../UTILITY/ENUMS");
const utility = require("../UTILITY/commonUtility");

const UserController = {
    /**
     * user signup
     * @param {*} req 
     * @param {*} res 
     */
    signup: async function(req, res) {
        try {           
            let username = req.body.username;
            let password = req.body.password;
            let email = req.body.email;

            if(!username || username.length < 3 || username.length > 32) {
                utility.sendResponse(res, 1100, "Invalid username, should be of 3 - 32 characters");
                return;
            }

            if(!password || password.length < 6 || password.length > 32) {
                utility.sendResponse(res, 1100, "Invalid password, should be of 6 - 32 characters");
                return;
            }

            if(!email || email.length < 5 || email.length > 255 || !utility.validateEmail(email)) {
                utility.sendResponse(res, 1100, "Invalid email");
                return;
            }
            let userObj = new User();
            let isEmailOrUserNameExist = await userObj.checkUsernameEmail(username, email);
            if(isEmailOrUserNameExist.length > 0 && (isEmailOrUserNameExist[0].username || isEmailOrUserNameExist[0].email)) {
                utility.sendResponse(res, 1100, "Username or Email already exists");
                return;
            }
            let result = await userObj.signUpUser(username, md5(password), email);
            if(result > 0) {
                jwt.sign({id: result, email, username, date: new Date().getTime()}, CONFIG.JWT_SECRET_KEY, (err, token) => {
                    if(err) {
                        utility.sendResponse(res, 1106, "Unable to create user");
                        return;
                    }
                    redisClient.getClient().set('token_' + token, result, 'EX', CONFIG.TOKEN_EXPIRE_TIME);
                    utility.sendResponse(res, ENUMS.responseCode.OK, "created user successfully", {
                        id: result,
                        username,
                        email,
                        token 
                    });
                })  
            } else {
                utility.sendResponse(res, 1106, "Unable to create user");
            }
        } catch(err) {
            console.error(err);
            utility.sendResponse(res, 1106, "Unable to create user");
        }
    },

    /**
     * user signin 
     * @param {*} req 
     * @param {*} res 
     */
    signin: async function(req, res) {
        try {
            let username = req.body.username;
            let password = req.body.password;

            if(!username || username.length < 3 || username.length > 255) {
                utility.sendResponse(res, 1200, "Invalid username or email");
                return;
            }

            if(!password || password.length < 6 || password.length > 32) {
                utility.sendResponse(res, 1200, "Invalid password, should be of 6 - 32 characters");
                return;
            }
            let userObj = new User();
            let result = await userObj.signInUser(username, md5(password));
            if(result.length > 0) {
                jwt.sign({id: result[0].id, email: result[0].email, username: result[0].username, date: new Date().getTime()}, CONFIG.JWT_SECRET_KEY, (err, token) => {
                    if(err) {
                        utility.sendResponse(res, 1200, "Unable to sign in user");
                        return;
                    }
                    redisClient.getClient().set('token_' + token, result[0].id, 'EX', CONFIG.TOKEN_EXPIRE_TIME);
                    utility.sendResponse(res, ENUMS.responseCode.OK, "successfully logged in", {
                        id: result[0].idult,
                        username: result[0].username,
                        email: result[0].email,
                        token 
                    });
                })  
            } else {
                utility.sendResponse(res, 1200, "Unable to signin user");
            }

        } catch(err) {
            console.error(err);
            utility.sendResponse(res, 1200, "Unable to signin user");
        }
    }
}

module.exports = UserController;