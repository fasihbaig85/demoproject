--
-- table user
--
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`(
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` varchar(32)  NOT NULL UNIQUE,
    `password` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL UNIQUE,
    KEY `UserIndex1` (`username`),
    KEY `UserIndex2` (`email`)
);


--
-- table articals
--
DROP TABLE IF EXISTS `articals`;
CREATE TABLE `articals`(
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `articalTitle` varchar(500) NOT NULL,
    `content` TEXT NOT NULL,
    `aurthorId` int(11) NOT NULL,
    `uploadedTime` varchar(15) NOT NULL,
    
    FOREIGN KEY (`aurthorId`) REFERENCES users(`id`)
);