Demo project of a nodejs server

Clone git repositiory
https://gitlab.com/fasihbaig85/demoproject.git

-install npm, nodejs, redis server and MYSQL SERVER latest versions

-run npm install

-you can also install redis server by executuion Extras/installRedisServer.sh

-create a database of name "demoDB"
-import schema.sql file from Schema folder
-Update database user name and password in config/config.js file

you can see some screenshots of API responses in API_RESPONSE_SCREENSHOTS