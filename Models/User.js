"use strict";
const db = require("../Database/mysqlConnection");

class User {
    constructor() {
        this.id = 0;
        this.username = "";
        this.password = "";
        this.email = "";
    }

    signUpUser(username, password, email) {
        this.username = username;
        this.password = password;
        this.email = email;
        return new Promise((resolve, reject) => {
            try {
                db.getConnection().then((connection) => {
                    connection.query(`INSERT INTO users (username, password, email) VALUES('${this.username}', '${this.password}', '${this.email}')`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        if(result.affectedRows == 1) {
                            resolve(result.insertId);
                        }
                        resolve(-1)
                    });
                }).catch(err => {
                    reject(-1)
                });
                
            } catch(err) {
                reject(500)
            }
        })
    }

    signInUser(username, password) {
        this.username = username;
        this.password = password;
        return new Promise((resolve, reject) => {
            try {
                db.getConnection().then((connection) => {
                    connection.query(`SELECT id, email, username FROM users WHERE (username = '${this.username}' OR  email = '${this.username}') and password = ${connection.escape(this.password)};`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        resolve(result);
                    });
                }).catch(err => {
                    reject(-1)
                });            
            } catch(err) {
                reject(err)
            }
        })   
    }

    checkUsernameEmail(username, email) {
        this.username = username;
        this.email = email;
        return new Promise((resolve, reject) => {
            try {
                db.getConnection().then((connection) => {
                    connection.query(`SELECT email, username FROM users WHERE username = '${this.username}' OR  email = '${this.email}';`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        resolve(result);
                    });
                }).catch(err => {
                    reject(err)
                });
                
            } catch(err) {
                reject(err)
            }
        })
    }

}

module.exports = User