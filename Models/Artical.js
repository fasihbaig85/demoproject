"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    Artical Model
*/
const db = require("../Database/mysqlConnection");

class Artical {
    /**
     * Constructor class Artical
     */
    constructor() {
        this.id = -1;
        this.articalTitle = "";
        this.content = "";
        this.aurthorId = "";
        this.uploadedTimestamp = -1;
        this.modifiedTimestamp = -1;
    }

    addArtical(articalTitle, content, aurthorId, uploadedTime) {
        this.articalTitle = articalTitle;
        this.aurthorId = aurthorId;
        this.content = content;
        this.uploadedTime = uploadedTime;

        return new Promise((resolve, reject) => {
            try {
                db.getConnection().then((connection) => {
                    connection.query(`INSERT INTO articals(articalTitle, aurthorId, content, uploadedTIme)
                                     VALUES(${connection.escape(this.articalTitle)}, ${this.aurthorId}, ${connection.escape(this.content)}, ${connection.escape(this.uploadedTime)});`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        if(result.affectedRows == 1) {
                            resolve(result.insertId);
                        }
                        resolve(-1)
                    });
                }).catch(err => {
                    reject(-1)
                });            
            } catch(err) {
                reject(500)
            }
        });
    }

    updateArtical(id, articalTitle, aurthorId, content) {
        this.id = id;
        this.articalTitle = articalTitle;
        this.aurthorId = aurthorId;
        this.content = content;

        return new Promise((resolve, reject) => {
            try {
                db.getConnection().then((connection) => {
                    connection.query(`UPDATE articals SET articalTitle = ${connection.escape(this.articalTitle)}, aurthorId = ${this.aurthorId}, 
                    content = ${connection.escape(this.content)} WHERE  id = ${this.id};`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        if(result.affectedRows >= 1) {
                            resolve(result.affectedRows);
                        }
                        resolve(-1)
                    });
                }).catch(err => {
                    reject(-1)
                });            
            } catch(err) {
                reject(500)
            }
        });
    }

    deleteArtical(ids) {
        return new Promise((resolve, reject) => {
            try {
                db.getConnection().then((connection) => {
                    connection.query(`DELETE FROM articals WHERE id IN (${ids});`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        if(result.affectedRows > 0) {
                            resolve(result.affectedRows);
                        }
                        resolve(-1)
                    });
                }).catch(err => {
                    reject(-1)
                });            
            } catch(err) {
                reject(500)
            }
        });
    }

    getArticalsByIds(ids) {
        return new Promise((resolve, reject) => {
            try {
                let whereClause = ''
                if(ids && ids.length) {
                    whereClause = `WHERE id IN (${ids})`;
                }
                db.getConnection().then((connection) => {
                    connection.query(`SELECT * FROM articals ${whereClause};`, (err, result) => {
                        connection.release();
                        if(err) {
                            reject(err);
                            return;
                        }
                        resolve(result);
                    });
                }).catch(err => {
                    reject(-1)
                });            
            } catch(err) {
                reject(500)
            }
        });
    }
}

module.exports = Artical