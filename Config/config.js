"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    config
*/
const CONFIG = {
    SERVER_PORT: 3000,
    DB_HOST: "localhost",
    DB_PORT: "3306",
    DB_USER_NAME: "{DB_UserName}",
    DB_NAME: "demoDB",
    DB_PASSWORD: "{DB_Password}",
    DB_PORT: "3306",
    DB_POOL_SIZE: 20,
    JWT_SECRET_KEY: "QW4HD-DQCRG-HM64M-6GJRK-8K83T",
    KEY_FILE_PATH: "./cert/server.key",
    CERT_FILE_PATH: "./cert/server.cert",
    TOKEN_EXPIRE_TIME: 3600 * 24 * 14,
    ARTICAL_FILE_PATH: './ArticalStore/ArticalStore.txt'
}

module.exports = CONFIG;