"use strict";

/*
    13-12-2020
    Created by fasih ullah 

    Routes 
*/

var express = require('express');
var router = express.Router();

var express = require('express');
var router = express.Router();

const UserController = require("../Controller/userController");
const ArticalController = require("../Controller/articalController");

router.post('/user/signup', UserController.signup);
router.post('/user/signin', UserController.signin);

router.get('/artical', ArticalController.getArtical)
router.post('/artical/add', ArticalController.addArtical);
router.put('/artical/update', ArticalController.updateArtical);
router.delete('/artical/delete/:ids', ArticalController.deleteArtical);

//router.post('/artical/add', ArticalController.getMulterConfiguration(), ArticalController.addArtical);
//router.put('/artical/update', ArticalController.getMulterConfiguration(), ArticalController.updateArtical);
//router.delete('/artical/delete/:ids', ArticalController.deleteArtical);

router.get('/', function(req, res, next) {
  res.status(200).send("demo work");
});

module.exports = router;
