const cluster = require('cluster');
const os = require('os');

http = require('http');
https = require('https');

const clutserMulticore = {
    createServer: (expressObj, opt) => {
        //console.log(process.argv);
        //console.log(os.cpus())
        const isClusterMode = os.cpus().length > 1;
        if(isClusterMode) {
            if(cluster.isMaster) {
                console.log(`Master ${process.pid} is runnig`);

                http.createServer((req, res) => {
                    res.writeHead(308, {"Location": `https://${req.headers['host']}${req.url}`});
                    res.end();
                }).listen(80);

                for (let index = 0; index < os.cpus().length ; index++) {
                    cluster.fork();
                }
                
                cluster.on('exit', (worker, code, signal) => {
                    console.log(`worker ${worker.process.pid} died`);
                    cluster.fork();
                });
            } else {
                let server = https.createServer(opt, expressObj);
                server.listen(443);

                server.on('error', onError);
                server.on('listening', onListening);
            }
        } else {
            let server = https.createServer(options, mainApp);
            server.listen(443);
            recurrentMeetingsHandler.startMeetingsPolling();
            http.createServer(function (req, res){
                res.writeHead(308, {"Location": `https://${req.headers['host']}${req.url}`});
                res.end();
            }).listen(80);

            server.on('error', onError);
            server.on('listening', onListening);
        }
    }
}


function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }
  
    var bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;
  
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
  
  /**
   * Event listener for HTTP server "listening" event.
   */
  
  function onListening() {
    // var addr = server.address();
    // var bind = typeof addr === 'string'
    //   ? 'pipe ' + addr
    //   : 'port ' + addr.port;
    console.log(`Server start listening pid ${process.pid}`);
  }

module.exports = clutserMulticore;